package com.afs.restapi;

import com.afs.restapi.pojo.Employee;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.mapper.EmployeeMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EmployeeJPARepository employeeJPARepository;


    @BeforeEach
    void setUp() {
        employeeJPARepository.deleteAll();
    }

    @Test
    void should_update_employee_age_and_salary() throws Exception {
        EmployeeRequest employeeRequestPrevious = getEmployeeRequestZhangsan();
        Employee employeePreviousSaved = employeeJPARepository.save(EmployeeMapper.toEntity(employeeRequestPrevious));

        Employee employeeUpdate = new Employee(employeePreviousSaved.getId(), "zhangsan", 24, "Male", 2000);

        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(employeeUpdate);
        mockMvc.perform(put("/employees/{id}", employeeUpdate.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().is(204))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(employeeUpdate.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(employeeUpdate.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(employeeUpdate.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").doesNotExist());
        Optional<Employee> optionalEmployee = employeeJPARepository.findById(employeeUpdate.getId());
        assertTrue(optionalEmployee.isPresent());
        Employee updatedEmployee = optionalEmployee.get();
        Assertions.assertEquals(employeeUpdate.getAge(), updatedEmployee.getAge());
        Assertions.assertEquals(employeeUpdate.getId(), updatedEmployee.getId());
        Assertions.assertEquals(employeeUpdate.getName(), updatedEmployee.getName());
        Assertions.assertEquals(employeeUpdate.getGender(), updatedEmployee.getGender());
    }

    @Test
    void should_create_employee() throws Exception {
        EmployeeRequest employeeRequest = getEmployeeRequestZhangsan();

        ObjectMapper objectMapper = new ObjectMapper();
        String employeeRequestString = objectMapper.writeValueAsString(employeeRequest);
        mockMvc.perform(post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(employeeRequestString))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(employeeRequest.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(employeeRequest.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(employeeRequest.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").doesNotExist());
    }
    @Test
    void should_find_employees() throws Exception {
        EmployeeRequest employeeRequest = getEmployeeRequestZhangsan();
        Employee employee = employeeJPARepository.save(EmployeeMapper.toEntity(employeeRequest));

        mockMvc.perform(get("/employees"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(employee.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employee.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employee.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employee.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").doesNotExist());
    }

    @Test
    void should_find_employee_by_id() throws Exception {
        EmployeeRequest employeeRequest = getEmployeeRequestZhangsan();
        Employee employeeSaved = employeeJPARepository.save(EmployeeMapper.toEntity(employeeRequest));

        mockMvc.perform(get("/employees/{id}", employeeSaved.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(employeeSaved.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(employeeSaved.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(employeeSaved.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(employeeSaved.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").doesNotExist());
    }

    @Test
    void should_delete_employee_by_id() throws Exception {
        EmployeeRequest employeeRequest = getEmployeeRequestZhangsan();
        Employee employee = employeeJPARepository.save(EmployeeMapper.toEntity(employeeRequest));

        mockMvc.perform(delete("/employees/{id}", employee.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(employeeJPARepository.findById(employee.getId()).isEmpty());
    }

    @Test
    void should_find_employee_by_gender() throws Exception {
        EmployeeRequest employeeRequest = getEmployeeRequestZhangsan();
        Employee employee = employeeJPARepository.save(EmployeeMapper.toEntity(employeeRequest));

        mockMvc.perform(get("/employees?gender={0}", "Male"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(employee.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employee.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employee.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employee.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").doesNotExist());
    }

    @Test
    void should_find_employees_by_page() throws Exception {
        EmployeeRequest employeeZhangsan = getEmployeeRequestZhangsan();
        EmployeeRequest employeeSusan = getEmployeeRequestSusan();
        EmployeeRequest employeeLisi = getEmployeeRequestLisi();
        Employee employeeZhangsanSaved = employeeJPARepository.save(EmployeeMapper.toEntity(employeeZhangsan));
        Employee employeeSusanSaved = employeeJPARepository.save(EmployeeMapper.toEntity(employeeSusan));
        employeeJPARepository.save(EmployeeMapper.toEntity(employeeLisi));

        mockMvc.perform(get("/employees")
                        .param("page", "1")
                        .param("size", "2"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(employeeZhangsanSaved.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employeeZhangsanSaved.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employeeZhangsanSaved.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employeeZhangsanSaved.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").doesNotExist())
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(employeeSusanSaved.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value(employeeSusanSaved.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].age").value(employeeSusanSaved.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].gender").value(employeeSusanSaved.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].salary").doesNotExist());
    }

    private static EmployeeRequest getEmployeeRequestZhangsan() {
        EmployeeRequest employeeRequest = new EmployeeRequest();
        employeeRequest.setName("zhangsan");
        employeeRequest.setAge(22);
        employeeRequest.setGender("Male");
        employeeRequest.setSalary(10000);
        return employeeRequest;
    }

    private static EmployeeRequest getEmployeeRequestSusan() {
        EmployeeRequest employee = new EmployeeRequest();
        employee.setName("susan");
        employee.setAge(23);
        employee.setGender("Male");
        employee.setSalary(11000);
        return employee;
    }

    private static EmployeeRequest getEmployeeRequestLisi() {
        EmployeeRequest employee = new EmployeeRequest();
        employee.setName("lisi");
        employee.setAge(24);
        employee.setGender("Female");
        employee.setSalary(12000);
        return employee;
    }
}