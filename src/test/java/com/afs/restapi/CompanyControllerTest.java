package com.afs.restapi;

import com.afs.restapi.pojo.Company;
import com.afs.restapi.pojo.Employee;
import com.afs.restapi.repository.CompanyJPARepository;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.mapper.CompanyMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class CompanyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CompanyJPARepository companyJPARepository;
    @Autowired
    private EmployeeJPARepository employeeJPARepository;

    @BeforeEach
    void setUp() {
        companyJPARepository.deleteAll();
        employeeJPARepository.deleteAll();
    }

    @Test
    void should_update_company_name() throws Exception {
        CompanyRequest previouscompanyRequest = getRequestCompany1();
        Company previousCompanySaved = companyJPARepository.save(CompanyMapper.toEntity(previouscompanyRequest));

        Company companyUpdateRequest = new Company(previousCompanySaved.getId(), "xyz");
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(companyUpdateRequest);

        mockMvc.perform(put("/companies/{id}", companyUpdateRequest.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().is(204))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(companyUpdateRequest.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.employeeCount").value(0));

        Optional<Company> optionalCompany = companyJPARepository.findById(companyUpdateRequest.getId());
        assertTrue(optionalCompany.isPresent());
        Company updatedCompany = optionalCompany.get();
        Assertions.assertEquals(previousCompanySaved.getId(), updatedCompany.getId());
        Assertions.assertEquals(companyUpdateRequest.getName(), updatedCompany.getName());
    }

    @Test
    void should_delete_company_name() throws Exception {
        CompanyRequest companyRequest = getRequestCompany1();
        Company company = companyJPARepository.save(CompanyMapper.toEntity(companyRequest));
        Employee employee = getEmployee(company);
        employeeJPARepository.save(employee);

        mockMvc.perform(delete("/companies/{id}", company.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(companyJPARepository.findById(company.getId()).isEmpty());
        assertTrue(employeeJPARepository.findById(employee.getId()).isEmpty());
    }

    @Test
    void should_create_company() throws Exception {
        CompanyRequest company = getRequestCompany1();

        ObjectMapper objectMapper = new ObjectMapper();
        String companyRequest = objectMapper.writeValueAsString(company);
        mockMvc.perform(post("/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(companyRequest))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(company.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty());
    }


    @Test
    void should_find_companies() throws Exception {
        CompanyRequest companyRequest = getRequestCompany1();
        Company company = companyJPARepository.save(CompanyMapper.toEntity(companyRequest));

        mockMvc.perform(get("/companies"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(company.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(company.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].employeeCount").value(0));
    }

    @Test
    void should_find_companies_by_page() throws Exception {
        CompanyRequest company1 = getRequestCompany1();
        CompanyRequest company2 = getRequestCompany2();
        CompanyRequest company3 = getRequestCompany3();
        Company companySaved1 = companyJPARepository.save(CompanyMapper.toEntity(company1));
        Company companySaved2 = companyJPARepository.save(CompanyMapper.toEntity(company2));
        companyJPARepository.save(CompanyMapper.toEntity(company3));

        mockMvc.perform(get("/companies")
                        .param("page", "1")
                        .param("size", "2"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(companySaved1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(company1.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].employeeCount").value(0))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(companySaved2.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value(company2.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].employeeCount").value(0))
        ;
    }

    @Test
    void should_find_company_by_id() throws Exception {
        CompanyRequest companyRequest1 = getRequestCompany1();
        Company companySaved = companyJPARepository.save(CompanyMapper.toEntity(companyRequest1));
        Employee employee = getEmployee(companySaved);

        employeeJPARepository.save(employee);

        mockMvc.perform(get("/companies/{id}", companySaved.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(companySaved.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(companySaved.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.employeeCount").value(1));
    }

    @Test
    void should_find_employees_by_companies() throws Exception {
        CompanyRequest companyRequest = getRequestCompany1();
        Company companySaved = companyJPARepository.save(CompanyMapper.toEntity(companyRequest));
        Employee employee = getEmployee(companySaved);
        employeeJPARepository.save(employee);

        mockMvc.perform(get("/companies/{companyId}/employees", companySaved.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(employee.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employee.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employee.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employee.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").doesNotExist());
    }

    private static Employee getEmployee(Company company) {
        Employee employee = new Employee();
        employee.setName("zhangsan");
        employee.setAge(22);
        employee.setGender("Male");
        employee.setSalary(10000);
        employee.setCompanyId(company.getId());
        return employee;
    }

    private static CompanyRequest getRequestCompany1() {
        CompanyRequest company = new CompanyRequest();
        company.setName("ABC");
        return company;
    }

    private static CompanyRequest getRequestCompany2() {
        CompanyRequest company = new CompanyRequest();
        company.setName("DEF");
        return company;
    }

    private static CompanyRequest getRequestCompany3() {
        CompanyRequest company = new CompanyRequest();
        company.setName("XYZ");
        return company;
    }

}