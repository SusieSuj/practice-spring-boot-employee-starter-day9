CREATE TABLE `companies` (
`id` bigint NOT NULL AUTO_INCREMENT,
`name` varchar(255) DEFAULT NULL,
PRIMARY KEY (`id`)
) ;


CREATE TABLE `employees` (
`id` bigint NOT NULL AUTO_INCREMENT,
`age` int DEFAULT NULL,
`company_id` bigint DEFAULT NULL,
`gender` varchar(255) DEFAULT NULL,
`name` varchar(255) DEFAULT NULL,
`salary` int DEFAULT NULL,
PRIMARY KEY (`id`),
FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`)
) ;

