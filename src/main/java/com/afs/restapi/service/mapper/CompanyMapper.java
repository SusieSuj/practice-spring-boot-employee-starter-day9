package com.afs.restapi.service.mapper;

import com.afs.restapi.pojo.Company;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import org.springframework.beans.BeanUtils;

public class CompanyMapper {
    private CompanyMapper() {
    }

    public static Company toEntity(CompanyRequest request){
        Company company = new Company();
        BeanUtils.copyProperties(request,company);
        return company;
    }

    public static CompanyResponse toResponse(Company company){
        CompanyResponse response = new CompanyResponse();
        response.setId(company.getId());
        response.setName(company.getName());
        if (company.getEmployees()!=null) {
            response.setEmployeeCount(company.computeEmployeeListSize());
        }
        return response;
    }
}
