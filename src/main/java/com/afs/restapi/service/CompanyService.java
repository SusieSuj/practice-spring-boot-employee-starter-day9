package com.afs.restapi.service;

import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.pojo.Employee;
import com.afs.restapi.repository.CompanyJPARepository;
import com.afs.restapi.repository.EmployeeJPARepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import com.afs.restapi.pojo.*;

import java.util.List;

@Service
public class CompanyService {

    private final EmployeeJPARepository employeeJPARepository;
    private final CompanyJPARepository companyJPARepository;

    public CompanyService( EmployeeJPARepository employeeJPARepository, CompanyJPARepository companyJPARepository) {
        this.employeeJPARepository = employeeJPARepository;

        this.companyJPARepository = companyJPARepository;
    }


    public List<Company> findAll() {
        return companyJPARepository.findAll();
    }

    public List<Company> findByPage(Integer page, Integer size) {
        return companyJPARepository.findAll(PageRequest.of(page - 1, size)).getContent();
    }

    public Company findCompanyById(Long id) {
        return companyJPARepository.findById(id).orElseThrow(CompanyNotFoundException::new);
    }

    public Company update(Long id, Company company) {
       Company companyUpdated = companyJPARepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        companyUpdated.setName(company.getName());
        companyJPARepository.save(companyUpdated);
        return companyUpdated;
    }

    public Company create(Company company) {
        return companyJPARepository.save(company);
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        return employeeJPARepository.findByCompanyId(id);
    }

    public void delete(Long id) {
        companyJPARepository.deleteById(id);
    }
}
