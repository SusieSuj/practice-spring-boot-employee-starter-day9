# O

- The topic of today's study is: SQL basic, JPS. 
- Something new I learned today : When a entity called Company has a list about another entity called Employee, if we want to use Java to create the two tables, we can add annotation: **@OneToMany** and **@JoinColumn** on the companyID in Employee, which is like create a foreign key in Employee.
- In today's homework,  there is a need of deleting a company. In the test, we just test that if the company is deleted. But we should know that when we delete a company, we may should delete the employees in this company. The solution is that we can use **@OneToMany(cascade = CascadeType.*REMOVE*, orphanRemoval = true)** .

# R

- I learned SQL before, so I feel SQL basic is easy. 

# I

- We use a database in memory to test the function, which will be faster than using a real database.
- There are many annotations, and there are many ways to use annotations.

# D

-  I can query for more annotations through a single annotation.

