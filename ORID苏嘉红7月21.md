# O

- The topic of today's study is: flyway, Spring boot mapper, microservice, container, CI CD,  cloud native
- Something new I learned today :
  -  Use flyway to record the database operations in our program. When we use flyway, the naming of sql files is strict. I couldn't successfully launch flyway at first, and then I found out that there was a problem with the SQL file naming: the first letter of the file naming is not capitalized, and there is no double underscore in the name. 
  - Use spring boot mapper to hide information that we don't want to be accessed. For example, if we don't want to send the salary information of the employee to the front, we can use a EmployeeResponse class, which don't have a salary property.
  - We had 3 sessions about microservice, container and CI CD. 

# R

- I felt a little confusing about CI CD and cloud native.

# I

- I don't know how to use microservice, container, CI CD and cloud native.

# D

- I should search for more materials.

